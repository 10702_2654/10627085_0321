package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import sun.nio.ch.Interruptible;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public ComboBox<String> cbBlood;
    public ComboBox<String> cbPlace;
    @FXML
    private RadioButton rbM;
    public RadioButton rbF;
    public RadioButton rbO;
    public ToggleGroup sexGroup;
    public DatePicker birthday;
    public ImageView myAvatar;
    public Button btnSelectAvatar;
    public GridPane myCtrl;

    public void doSelectBirthday(ActionEvent actionEvent) {
        System.out.println(birthday.getValue());
    }

    public void doSelectSex(ActionEvent actionEvent) {
        RadioButton radioButton = (RadioButton) actionEvent.getSource();
        System.out.println(radioButton.getText());
    }

    public void doSelectAvatar(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Picture", "*.jpg", "*.gif", "*.png")
        );
        File selectedFile = fileChooser.showOpenDialog(myCtrl.getScene().getWindow());
        System.out.println(selectedFile.getAbsolutePath());

        myAvatar.setImage(new Image(selectedFile.toURI().toString()));
    }

    @Override
    public void initialize(URL location, ResourceBundle resource) {
        ObservableList<String> items = FXCollections.observableArrayList();
        items.addAll("A", "B", "O", "Other");
        cbBlood.getItems().addAll(items);

        ObservableList<String> itemplaces = FXCollections.observableArrayList();
        String filename = "D:\\user\\Desktop\\10627085_0314\\place.txt";
        String line;
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            while ((line = bufferedReader.readLine()) != null) {
                itemplaces.add(line);
            }
            bufferedReader.close();
            cbPlace.setItems(itemplaces);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}



